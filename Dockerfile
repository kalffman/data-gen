FROM adoptopenjdk/openjdk11:alpine-jre-nightly

RUN mkdir /opt/api

WORKDIR /opt/api

COPY target/*.jar app.jar

EXPOSE 8080

ENTRYPOINT ["java", "-jar", "app.jar"]