package br.gov.rr.digigov.projetos.datagenerator.controller;

import br.gov.rr.digigov.projetos.datagenerator.model.Pessoa;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gargoylesoftware.htmlunit.ScriptException;
import com.gargoylesoftware.htmlunit.SilentCssErrorHandler;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequestMapping("/pessoas")
public class GeradorPessoaController {

    private HtmlPage page;
    private ObjectMapper mapper;

    @PostConstruct
    public void init() {
        WebClient client = new WebClient();
        client.getOptions().setCssEnabled(false);
        client.getOptions().setJavaScriptEnabled(true);
        client.getOptions().setThrowExceptionOnFailingStatusCode(false);
        client.getOptions().setThrowExceptionOnScriptError(false);
        client.getOptions().setTimeout(5000);
        client.setCssErrorHandler(new SilentCssErrorHandler());
        try {
            String URL = "https://www.4devs.com.br/gerador_de_pessoas";
            page = client.getPage(URL);
            log.info("**********************************************************************");
            log.info("********************* PAGINA CARREGADA COM EXITO *********************");
            log.info("**********************************************************************\n");
            mapper = new ObjectMapper();
        } catch (Exception e) {
            log.error(e.getMessage());
        }

        log.info("***********************************************************************");
        log.info("Rotas disponíveis******************************************************");
        log.info("/generator/pessoas/*(GET)**********************************************");
        log.info("**Parametros***********************************************************");
        log.info("****estado:   String (unidade federativa. Ex: SP, CE, AM)**************");
        log.info("****qtd:      Integer (quantidade de pessoas a serem geradas. Max: 30)*");
        log.info("****formatado:Boolean (para campos com ou sem mascara. Default: true)**");
        log.info("***********************************************************************");
    }

    @GetMapping
    public ResponseEntity<?> getPessoas(@Nullable @RequestParam("estado") String estado,
                                        @Nullable @RequestParam("qtd") Integer qtd,
                                        @Nullable @RequestParam("formatado") Boolean formatado) {
        List<Pessoa> pessoas = new ArrayList<>();
        if (qtd == null) { qtd = 1; }
        if (formatado == null) { formatado = true; }

        try {
            HtmlButtonInput inputGerar = (HtmlButtonInput) page.getElementById("bt_gerar_pessoa");

            HtmlSelect selectEstado = (HtmlSelect) page.getElementById("cep_estado");
            if (estado != null) {
                if (selectEstado.getOptions().stream().map(HtmlOption::getValueAttribute).collect(Collectors.toList()).contains(estado)) {
                    HtmlOption optionEstado = selectEstado.getOptionByValue(estado);
                    selectEstado.setSelectedAttribute(optionEstado, true);
                } else {
                    return ResponseEntity.badRequest().body("Estado inválido. \"" + estado + "\" não existe");
                }
            }

            HtmlNumberInput inputQqt = (HtmlNumberInput) page.getElementById("txt_qtde");
            if (qtd <= 30 && qtd >= 1) {
                inputQqt.setValueAttribute(String.valueOf(qtd));
            } else {
                return ResponseEntity.badRequest().body("O valor da quantidade de pessoas deve ser entre 1 e 30. \"" + qtd + "\" inválido");
            }

            HtmlRadioButtonInput pontuacao = (HtmlRadioButtonInput) (formatado ?  page.getElementById("pontuacao_sim") : page.getElementById("pontuacao_nao"));
            pontuacao.setChecked(true);

            HtmlPage result = inputGerar.click();

            String jsonBody = null;
            while (jsonBody == null) {
                HtmlTextArea textAreaJson = (HtmlTextArea) result.getElementById("dados_json");

                String text = textAreaJson.getText();
                if(text.equals("Gerando pessoas, por favor aguarde...") || StringUtils.isEmpty(text)) {
                    log.info(text);
                    Thread.sleep(500);
                } else {
                    jsonBody = textAreaJson.getText();
                }
            }

            if (qtd == 1)
                pessoas.add(mapper.readValue(jsonBody, Pessoa.class));
            else
                pessoas.addAll(mapper.readValue(jsonBody, mapper.getTypeFactory().constructCollectionType(List.class, Pessoa.class)));

            return ResponseEntity.ok(pessoas);
//            return ResponseEntity.ok(Collections.EMPTY_LIST);
        } catch (Exception e) {
            log.error(e.getMessage());
            return ResponseEntity.ok(Collections.EMPTY_LIST);
        }
    }
}
