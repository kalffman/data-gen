package br.gov.rr.digigov.projetos.datagenerator.model;

import br.gov.rr.digigov.projetos.datagenerator.deserializer.PessoaDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.LocalDate;

@Getter
@AllArgsConstructor
@JsonDeserialize(using = PessoaDeserializer.class)
public class Pessoa {

    private final String nome;
    private final Integer idade;
    private final String cpf;
    private final String rg;
    private final LocalDate dataNasc;
    private final String signo;
    private final String mae;
    private final String pai;
    private final String email;
    private final String senha;
    private final String cep;
    private final String endereco;
    private final Integer numero;
    private final String bairro;
    private final String cidade;
    private final String uf;
    private final String telefoneFixo;
    private final String celular;
    private final String altura;
    private final Integer peso;
    private final String tipoSanguineo;
    private final String rh;
    private final String corFavorita;

}
