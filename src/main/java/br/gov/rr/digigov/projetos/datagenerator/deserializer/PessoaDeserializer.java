package br.gov.rr.digigov.projetos.datagenerator.deserializer;

import br.gov.rr.digigov.projetos.datagenerator.model.Pessoa;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.*;
import org.apache.tomcat.jni.Local;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class PessoaDeserializer extends StdDeserializer<Pessoa> {
    private final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");


    public PessoaDeserializer() {
        super((JavaType) null);
    }

    public PessoaDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public Pessoa deserialize(JsonParser jp, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        JsonNode node = jp.getCodec().readTree(jp);

        String nome = node.get("nome").asText();
        Integer idade = node.get("idade").intValue();
        String cpf = node.get("cpf").asText();
        String rg = node.get("rg").asText();
        LocalDate dataNascimento = LocalDate.parse(node.get("data_nasc").asText(), dtf);
        String signo = node.get("signo").asText();
        String mae = node.get("mae").asText();
        String pai = node.get("pai").asText();
        String email = node.get("email").asText();
        String senha = node.get("senha").asText();
        String cep = node.get("cep").asText();
        String endereco = node.get("endereco").asText();
        Integer numero = node.get("numero").intValue();
        String bairro = node.get("bairro").asText();
        String cidade = node.get("cidade").asText();
        String uf = node.get("estado").asText();
        String telefoneFixo = node.get("telefone_fixo").asText();
        String celular = node.get("celular").asText();
        String altura = node.get("altura").asText();
        Integer peso = node.get("peso").intValue();
        String tipoSanguineo = node.get("tipo_sanguineo").asText();
        String rh = tipoSanguineo.substring(tipoSanguineo.length() - 1);

        tipoSanguineo = tipoSanguineo.replace(rh, "");

        String corFavorita = node.get("cor").asText();

        return new Pessoa(
                nome,
                idade,
                cpf,
                rg,
                dataNascimento,
                signo,
                mae,
                pai,
                email,
                senha,
                cep,
                endereco,
                numero,
                bairro,
                cidade,
                uf,
                telefoneFixo,
                celular,
                altura,
                peso,
                tipoSanguineo,
                rh,
                corFavorita
        );
    }
}
